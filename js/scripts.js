$(document).ready(function() {
    
    /* Every time the window is scrolled ... */
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.hideme').each( function(){

            var bottom_of_object = $(this).position().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},1000);
                    
            }
        }); 
    });
});

document.querySelector('.hamburgermenu').addEventListener('click', function(){
	document.querySelector('nav').classList.toggle('open');

})

document.querySelector('.hamburgermenu').addEventListener('click', function(){
	document.querySelector('close').classList.toggle('open');

})

function myFunction(x) {
  x.classList.toggle("change");
}

$(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    });
});

$("#gif-rows").hover(function() {
  $('.preset-file').toggle();
  $('.gif-file').toggle();
});

// Home Page Counter
	
	$('.counter-count').each(function () {
        var currentElement = $(this).prop('Counter',0)

        // currentElement.animate({Counter: 22},{})

        currentElement.animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
            	console.log(now)
                $(this).text(Math.ceil(now));
            }
        });
    });